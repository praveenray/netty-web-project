package biz.yellowfish.handlers;

import io.netty.channel.SimpleChannelInboundHandler;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelLocal;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.QueryStringDecoder;
import org.jboss.netty.util.CharsetUtil;

import java.lang.reflect.Method;
import java.util.*;
public class ReadHttpParamsHandler extends SimpleChannelInboundHandler {
}

public class ReadHttpParamsHandler extends SimpleChannelUpstreamHandler {
    public static final String JSON_DATA="json";
    public enum STATUS {
        OK,
        STOP
    }
    public static final ChannelLocal<Map<String, List<String>>> HTTP_PARAMETERS = new ChannelLocal<Map<String, List<String>>>() {
        @Override
        protected Map<String, List<String>> initialValue(Channel channel) {
            return new HashMap<String, List<String>>();
        }
    };

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        HttpRequest request = (HttpRequest) e.getMessage();
        HttpMethod httpMethod = request.getMethod();
        Map<String, List<String>> parameters = null;
        if (httpMethod == HttpMethod.GET) {
            parameters = this.processGETParameters(request);
        } else if (httpMethod == HttpMethod.POST) {
            parameters = this.processPOSTParameters(request);
        }

        HTTP_PARAMETERS.set(ctx.getChannel(), parameters);
        ctx.sendUpstream(e);
    }

    /**
     * Compresses list param values into scalar.
     * @return <Map<String, List<String>>>
     */
    public static Map<String,Object> compressListParams(Map<String, List<String>> params) {
        Map<String,Object> compressed = new HashMap<String,Object>();
        for(Map.Entry<String,List<String>> entry : params.entrySet()) {
            List<String> values = entry.getValue();
            if (values != null) {
                compressed.put(entry.getKey(), values.size() > 1 ? values : values.get(0));
            }
        }
        return compressed;
    }
    
    private Map<String, List<String>> processPOSTParameters(HttpRequest request) {
        try {
            ChannelBuffer content = request.getContent();

            String state = "start";
            ReturnTuple returnTuple = null;
            int i=0;
            while (true) {
                Method method = this.getClass().getDeclaredMethod(state, byte.class, HttpRequest.class, ReturnTuple.class);
                returnTuple = (ReturnTuple) method.invoke(this, (i < content.capacity()) ? content.getByte(i++) : ((byte)0), request, returnTuple);
                state = returnTuple.getNextState();
//                System.out.println("state: "+(state != null ? state : "no state") + " returnTuple "+ returnTuple);
                if (returnTuple.getStatus() == STATUS.STOP) {
                    break;
                }
            }
            return returnTuple.getParameters();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Map<String, List<String>> processGETParameters(HttpRequest request) {
        QueryStringDecoder decoder = new QueryStringDecoder(request.getUri());
        return Collections.unmodifiableMap(decoder.getParameters());
    }

    private ReturnTuple start(byte nextByte, HttpRequest request, ReturnTuple currentState) {
        String contentType = request.getHeader(HttpHeaders.Names.CONTENT_TYPE);
        if (contentType.equals(HttpHeaders.Values.APPLICATION_X_WWW_FORM_URLENCODED)) {
            return new ReturnTuple(STATUS.OK, "collectUrlEncodedEntity", nextByte);
        } else if (contentType.startsWith(HttpHeaders.Values.MULTIPART_FORM_DATA)) {
            return new MultipartFormState(STATUS.OK, "startMultipartFormEntity", nextByte, this.extractBoundry(request));
        } else if (contentType.equals("application/json")) {
            return new JSONPayload(STATUS.OK, "readJsonByte", nextByte, request.getContent().readableBytes());
        }
        throw new IllegalStateException("Invalid Content-Type : "+ contentType);
    }
    
    private ReturnTuple readJsonByte(byte nextByte, HttpRequest request, ReturnTuple currentState) {
        JSONPayload payload = (JSONPayload) currentState;
        if (payload.getTotalBytes() == payload.getBuffer().length) {
            return new JSONPayload(STATUS.OK, "processJsonPayload", currentState.getBuffer(), payload.getTotalBytes());
        }
        return new JSONPayload(STATUS.OK, "readJsonByte", nextByte, currentState.getBuffer(), payload.getTotalBytes());
    }

    private ReturnTuple processJsonPayload(byte nextByte, HttpRequest request, ReturnTuple currentState) {
        String json = new String(currentState.getBuffer(), CharsetUtil.UTF_8);
        Map<String,List<String>> data = new HashMap<String, List<String>>();
        data.put(JSON_DATA, Arrays.asList(json));
        return new JSONPayload(data, ((JSONPayload) currentState).getTotalBytes());
    }
    
    private ReturnTuple collectUrlEncodedEntity(byte nextByte, HttpRequest request, ReturnTuple currentState) {
        long contentLength = HttpHeaders.getContentLength(request);
        if (contentLength == currentState.getBuffer().length) {
            return new ReturnTuple(STATUS.OK, "processUrlEncodedEntity", currentState.getBuffer());
        }
        return new ReturnTuple(STATUS.OK, "collectUrlEncodedEntity", currentState.getBuffer(), nextByte);
    }

    private ReturnTuple processUrlEncodedEntity(byte nextByte, HttpRequest request, ReturnTuple currentState) {
        String query = new String(currentState.getBuffer(), CharsetUtil.UTF_8);
        if (!query.startsWith("?")) {
            query = "?" + query;
        }
        QueryStringDecoder decoder = new QueryStringDecoder(query);
        return new ReturnTuple(decoder.getParameters());
    }

    private ReturnTuple startMultipartFormEntity(byte nextByte, HttpRequest request, ReturnTuple currentState) {
        MultipartFormState state = (MultipartFormState) currentState;
        byte[] startSignature = state.getBoundry();
        if (this.endsWith(state.getBuffer(), startSignature, new int[]{0x0d, 0x0a})) {
            return new MultipartFormState(STATUS.OK, "collectContentDisposition", nextByte, state.getBoundry());
        }
        return new MultipartFormState(STATUS.OK, "startMultipartFormEntity", currentState.getBuffer(), nextByte, state.getBoundry());
    }

    private boolean endsWith(byte[] buffer, byte[] signatureAtEnd, byte... extraBytesAtEnd) {
        byte[] endSignature = ArrayUtils.addAll(signatureAtEnd, extraBytesAtEnd);
        if (buffer.length < endSignature.length) {
            return false;
        }

        byte[] end = ArrayUtils.subarray(buffer, buffer.length-endSignature.length, buffer.length);
        EqualsBuilder eq = new EqualsBuilder().append(end, endSignature);
        return eq.isEquals();
    }

    private boolean endsWith(byte[] buffer, byte[] signatureAtEnd, int... extraBytesAtEnd) {
        byte[] extra = new byte[extraBytesAtEnd.length];
        for(int i=0; i < extraBytesAtEnd.length; i++) {
            extra[i] = (byte) extraBytesAtEnd[i];
        }
        return this.endsWith(buffer, signatureAtEnd, extra);
    }
    
    private ReturnTuple collectContentDisposition(byte nextByte, HttpRequest request, ReturnTuple currentState) {
        MultipartFormState state = (MultipartFormState) currentState;
        String buffer = new String(state.getBuffer(), CharsetUtil.UTF_8);
        if (buffer.equals("Content-Disposition: form-data; ")) {
            return new MultipartFormState(STATUS.OK, "collectParameterName", nextByte, state.getBoundry(), state.getParameters());
        }
        return new MultipartFormState(STATUS.OK, "collectContentDisposition", state.getBuffer(), nextByte, state.getBoundry(), state.getParameters());
    }

    private ReturnTuple collectParameterName(byte nextByte, HttpRequest request, ReturnTuple currentState) {
        MultipartFormState state = (MultipartFormState) currentState;
        byte[] buffer = state.getBuffer();
        if (this.endsWith(buffer, new byte[]{}, 0x0d, 0x0a, 0x0d, 0x0a)) {
            String nameValue = new String(ArrayUtils.subarray(buffer , 0, buffer.length - 4), CharsetUtil.UTF_8);
            String[] tokens = nameValue.split("=");
            assert tokens.length == 2 : "Name Value Pair " + nameValue +"is not valid";
            assert tokens[0].equals("name") : "form name value pair must start with 'name'";
            String paramName = tokens[1].trim().substring(1, tokens[1].length()-1);// remove first and last '"' characters
            return new MultipartFormState(STATUS.OK, "collectParameterValue", nextByte, state.getBoundry(), state.getParameters(), paramName);
        }
        
        return new MultipartFormState(STATUS.OK, "collectParameterName", buffer, nextByte, state.getBoundry(), state.getParameters());
    }
    
    private ReturnTuple collectParameterValue(byte nextByte, HttpRequest request, ReturnTuple currentState) {
        MultipartFormState state = (MultipartFormState) currentState;
        byte[] buffer = state.getBuffer();
        byte[] endSignature = ArrayUtils.addAll(new byte[]{0x0d, 0x0a}, state.getBoundry());
        byte[] dashesAtEnd = new byte[]{'-','-'};
        if (this.endsWith(buffer, endSignature, new int[]{0x0d, 0x0a}) || this.endsWith(buffer, endSignature, dashesAtEnd)) {
            byte[] value = ArrayUtils.subarray(buffer, 0, buffer.length - (endSignature.length+2));// account for 2 CRLF bytes (or 2 -- bytes) at end
            STATUS status = (this.endsWith(buffer, dashesAtEnd, new int[]{})) ? STATUS.STOP : STATUS.OK;
            return new MultipartFormState(status, "collectContentDisposition", nextByte, state.getBoundry(), state.getParameters(), state.getCurrentParamName(), value);
        }
        
        return new MultipartFormState(STATUS.OK, "collectParameterValue", buffer, nextByte, state.getBoundry(), state.getParameters(), state.getCurrentParamName());
    }
    
    private byte[] extractBoundry(HttpRequest request) {
        String contentType = request.getHeader(HttpHeaders.Names.CONTENT_TYPE);
        String[] tokens = contentType.split(";");
        String[] boundaryTokens = tokens[1].trim().split("=");
        return ArrayUtils.addAll(new byte[]{'-','-'}, boundaryTokens[1].trim().getBytes(CharsetUtil.US_ASCII)); //add two dashes in front
    }

    private static class JSONPayload extends ReturnTuple{
        private int totalBytes;

        public JSONPayload(STATUS status, String nextstate, Byte firstChar, int totalByteCount) {
            super(status,nextstate,firstChar);
            this.totalBytes = totalByteCount;
        }

        public JSONPayload(STATUS status, String  nextState, byte nextByte, byte[] buffer, int totalByteCount) {
            super(status, nextState, buffer, nextByte);
            this.totalBytes = totalByteCount;
        }

        public JSONPayload(STATUS status, String  nextState, byte[] buffer, int totalByteCount) {
            super(status,nextState,buffer);
            this.totalBytes = totalByteCount;
        }

        public JSONPayload(Map<String,List<String>> data, int totalByteCount) {
            super(data);
            this.totalBytes = totalByteCount;
        }
        
        public int getTotalBytes() {
            return totalBytes;
        }
    }
    
    private static class MultipartFormState extends ReturnTuple{
        private byte[] boundry;
        private String currentParamName;

        public MultipartFormState(STATUS status, String nextstate, Byte firstChar, byte[] boundry) {
            super(status, nextstate, firstChar);
            this.boundry = boundry;
        }

        public MultipartFormState(STATUS status, String nextstate, Byte firstChar, byte[] boundry, Map<String, List<String>> existingParams) {
            super(status, nextstate, firstChar);
            this.setParameters(this.copyParameters(existingParams));
            this.boundry = boundry;
        }

        public MultipartFormState(STATUS status, String nextstate, byte[] existingBuffer, byte newByte, byte[] boundry) {
            super(status, nextstate, existingBuffer, newByte);
            this.boundry = boundry;
        }

        public MultipartFormState(STATUS status, String nextstate, byte[] existingBuffer, byte newByte, byte[] boundry, Map<String, List<String>> existingParams) {
            super(status, nextstate, existingBuffer, newByte);
            this.setParameters(this.copyParameters(existingParams));
            this.boundry = boundry;
        }

        public MultipartFormState(STATUS status, String nextstate, Byte nextChar, byte[] boundry, Map<String, List<String>> existingParams, String newParamName) {
            this(status, nextstate, nextChar, boundry);
            this.setParameters(this.copyParameters(existingParams));
            this.currentParamName = newParamName;
        }

        public MultipartFormState(STATUS status, String nextstate, byte[] buffer, Byte nextChar, byte[] boundry, Map<String, List<String>> existingParams, String newParamName) {
            this(status, nextstate, buffer, nextChar, boundry);
            this.setParameters(this.copyParameters(existingParams));
            this.currentParamName = newParamName;
        }

        public MultipartFormState(STATUS status, String nextstate, Byte nextChar, byte[] boundry, Map<String, List<String>> existingParams, String paramName, byte[] paramValue) {
            this(status, nextstate, nextChar, boundry);            
            this.setParameters(this.copyParameters(existingParams));
            this.addValues(this.getParameters(), paramName, paramValue);
        }

        private void addValues(Map<String, List<String>> myParameters, String paramName, byte[] paramValue) {
            List<String> values = myParameters.get(paramName);
            if (values == null) {
                values = new ArrayList<String>();
                myParameters.put(paramName, values);
            }
            values.add(new String(paramValue, CharsetUtil.UTF_8));
        }
                
        public byte[] getBoundry() {
            return boundry;
        }

        public String getCurrentParamName() {
            return currentParamName;
        }

        private Map<String, List<String>> copyParameters(Map<String, List<String>> existingParams) {
            Map<String,List<String>> map = new HashMap<String, List<String>>();
            if (existingParams != null) {
                for(Map.Entry<String,List<String>> entry : existingParams.entrySet()) {
                    map.put(entry.getKey(), entry.getValue());
                }
            }
            return map;
        }

    }
    
    private static class ReturnTuple {
        private String nextState;
        private STATUS status;
        private byte[] buffer;

        private Map<String, List<String>> parameters;

        public ReturnTuple(Map<String, List<String>> p) {
            this.status = STATUS.STOP;
            this.parameters = Collections.unmodifiableMap(p);
        }
        public ReturnTuple(STATUS status, String nextstate, Byte firstChar) {
            this.status = status;
            this.nextState = nextstate;
            this.buffer = new byte[1];
            this.buffer[0] = firstChar;
        }
        
        public ReturnTuple(STATUS status, String nextstate, byte[] existingBuffer) {
            this.status = status;
            this.nextState = nextstate;
            this.buffer =  ArrayUtils.clone(existingBuffer);
        }

        public ReturnTuple(STATUS status, String nextstate, byte[] existingBuffer, byte newByte) {
            this.status = status;
            this.nextState = nextstate;
            this.buffer = new byte[existingBuffer.length + 1];
            for (int i=0; i < existingBuffer.length; i++) {
                this.buffer[i] = existingBuffer[i];
            }
            this.buffer[existingBuffer.length] = newByte;
        }

        public byte[] getBuffer() {
            return ArrayUtils.clone(this.buffer);
        }

        public String getNextState() {
            return nextState;
        }

        public STATUS getStatus() {
            return status;
        }

        public Map<String, List<String>> getParameters() {
            return parameters;
        }
        
        protected void setParameters(Map<String,List<String>> p) {
            this.parameters = p;
        }

        @Override
        public String toString() {
            String buf = "";
            if (this.buffer != null) {
                buf = new String(this.buffer, CharsetUtil.UTF_8);
            } else {
                buf = "NOTHING IN RETURNTUPLE";
            }
            return this.status + ":" + this.nextState + ":" + buf + ":" + this.parameters;
        }
    }
    
    private static class NameValuePair {
        private String name;
        private String value;

        public NameValuePair(String name, String value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }
}
