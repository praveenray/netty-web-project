package biz.yellowfish.jmx;

import biz.yellowfish.services.Utils;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.annotation.Annotation;
import java.lang.management.ManagementFactory;

/***
 * Finds all classes starting in the root package which have @biz.yellowfish.jmx.JMXBean annotation , creates an instance and registers with default JMXServer
 * Note that this creates an instance using newInstance()
 * TODO: Inject a DI container so we don't need to create an object here
 */
public class JMXDiscoverer  {
    public void init(Utils utils) {
        try {
            MBeanServer beanServer = ManagementFactory.getPlatformMBeanServer();
            String pkgName = this.getClass().getPackage().getName().replaceAll("\\.jmx$", "");
            Class[] classes = utils.get_classes_in_a_package(pkgName);
            for (Class cls : classes) {
                Annotation jmxBean = cls.getAnnotation(JMXBean.class);
                if (jmxBean != null) {
                    ObjectName name = new ObjectName(String.format("%s:type=%s", pkgName, cls.getName()));
                    beanServer.registerMBean(cls.newInstance(), name);
                    System.out.println("Registered MBEAN "+name);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
