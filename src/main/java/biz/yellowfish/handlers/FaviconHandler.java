package biz.yellowfish.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.FullHttpRequest;

/**
 * Install after HttpAggregator. This simply handles requests looking for favicon.ico
 */
public class FaviconHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof FullHttpRequest) {
            FullHttpRequest request = (FullHttpRequest) msg;
            if (request.getUri().equals("/favicon.ico")) {
                ctx.channel().close();
                request.release();
            }
        } else {
            super.channelRead(ctx, msg);
        }
    }
}
