package biz.yellowfish.handlers;

import biz.yellowfish.services.JaxControllerDispatcher;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.CharsetUtil;

public class ControllerManager extends SimpleChannelInboundHandler {
    private JaxControllerDispatcher cm;

    public ControllerManager(JaxControllerDispatcher cm) {
        this.cm = cm;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
        HttpResponse response = null;
        if (msg instanceof FullHttpRequest) {
            response = this.cm.handleRequest((FullHttpRequest) msg, ctx);
        }

        if (response == null) {
            ByteBuf buffer = Unpooled.copiedBuffer("Nothing to see here!", CharsetUtil.UTF_8);
            response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, buffer);
            response.headers().set(HttpHeaders.Names.CONTENT_TYPE, "text/plain");
        }
        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }
}
