package biz.yellowfish.handlers;

import org.apache.commons.logging.Log;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelFutureProgressListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.DefaultFileRegion;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.FileRegion;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.handler.codec.http.DefaultHttpResponse;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.jboss.netty.handler.codec.http.HttpVersion;
import org.jboss.netty.handler.codec.http.QueryStringDecoder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class StaticContentHandler extends SimpleChannelUpstreamHandler {
    public static final String ASSETS_PREFIX="/assets";
    private File docroot;
    private Log log;

    public StaticContentHandler(String doc_root, Log logger) {
        this.log = logger;
        File f = new File(doc_root);
        if (!f.exists() || !f.isDirectory()) {
            throw new IllegalArgumentException("Invalid doc root " + doc_root);
        }
        this.docroot = f;
    }
    
    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        HttpRequest request = (HttpRequest) e.getMessage();

        QueryStringDecoder decoder = new QueryStringDecoder(request.getUri());
        String uri = decoder.getPath().trim();

        try {
            if (uri.startsWith(ASSETS_PREFIX))  {
                File staticFile =  this.getFilePath(uri);
                this.log.debug("Serving static File "+staticFile.getAbsolutePath());
                this.serveStaticFile(staticFile, ctx);
            } else {
                ctx.sendUpstream(e);
            }
        } catch (FileNotFoundException exp) {
            this.log.warn("URI " + uri +" Not found");
            HttpResponse response = new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_FOUND);
            ctx.getChannel().write(response).addListener(ChannelFutureListener.CLOSE);            
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        Throwable cause = e.getCause();
        if (cause instanceof FileNotFoundException) {
            HttpResponse response = new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_FOUND);
            ctx.getChannel().write(response).addListener(ChannelFutureListener.CLOSE);
        }
    }

    private void serveStaticFile(File file, ChannelHandlerContext ctx) {
        try {
            HttpResponse response = new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);

            org.jboss.netty.channel.Channel ch = ctx.getChannel();
            String fileName = file.getName();
            if (fileName.endsWith(".js")) {
                response.setHeader(HttpHeaders.Names.CONTENT_TYPE, "text/javascript");
            } else if (fileName.endsWith(".css")) {
                response.setHeader(HttpHeaders.Names.CONTENT_TYPE, "text/css");
            } else if (fileName.endsWith(".html")) {
                response.setHeader(HttpHeaders.Names.CONTENT_TYPE, "text/html");
            } else if (fileName.endsWith("jpg")) {
                response.setHeader(HttpHeaders.Names.CONTENT_TYPE, "image/jpeg");
            } else if (fileName.endsWith("png")) {
                response.setHeader(HttpHeaders.Names.CONTENT_TYPE, "image/png");
            } else if (fileName.endsWith("gif")) {
                response.setHeader(HttpHeaders.Names.CONTENT_TYPE, "image/gif");
            }

            response.setHeader(HttpHeaders.Names.CACHE_CONTROL,  String.format("max-age=%d", 24*60*60));

            RandomAccessFile raf = new RandomAccessFile(file, "r");
            long fileLength = raf.length();
            response.setHeader(HttpHeaders.Names.CONTENT_LENGTH, fileLength);
            ch.write(response);

            final FileRegion region = new DefaultFileRegion(raf.getChannel(), 0, fileLength);
            ChannelFuture writeFuture = ch.write(region);
            writeFuture.addListener(new ChannelFutureProgressListener() {
                @Override
                public void operationProgressed(ChannelFuture future, long amount, long current, long total) throws Exception {
                    System.out.println("Uploaded "+current+" of "+total);
                }

                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    region.releaseExternalResources();
                }
            });
            writeFuture.addListener(ChannelFutureListener.CLOSE);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private File getFilePath(String uri) throws IOException {
        File file = resolveFile(this.docroot, uri);
        if (file == null) {
            throw new FileNotFoundException("URI " + uri + " not found");
        }
        return file;
    }

    private static String replaceAssetPrefix(String uri) {
        return uri.replaceAll("^"+ASSETS_PREFIX ,"").replaceAll("^/","").replace("/", File.separator);
    }
    public static File resolveFile(File docroot, String uri) {
        String path = replaceAssetPrefix(uri);
        try {
            File file = (new File(docroot.getAbsolutePath() + File.separator + path)).getCanonicalFile();
            return file.exists() ? file : null;
        } catch (IOException e) {
            return null;
        }
    }
}
