package biz.yellowfish.handlers;

import biz.yellowfish.jmx.JMXBean;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.util.logging.Logger;


/**
 * Use for debugging only. Synchronizes on itself
 */
@JMXBean
public class DumpRawRequest extends ChannelInboundHandlerAdapter {

    private Path file;
    private static final Logger log = Logger.getLogger(DumpRawRequest.class.getName());

    public DumpRawRequest(Path fileToWrite) {
        this.file =  fileToWrite;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof ByteBuf) {
            ByteBuf buffer = (ByteBuf) msg;
            int bytesToRead = buffer.readableBytes();
            if (bytesToRead > 0) {
                byte[] bytes = new byte[bytesToRead];
                buffer.readBytes(bytes,0,bytesToRead);
                this.writeToFile(ctx.channel().remoteAddress().toString(), bytes);
            }
        }
        super.channelRead(ctx, msg);
    }

    private synchronized void writeToFile(String remoteAddress, byte[] bytebuffer) {
        try {
            BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(this.file.toFile(), true));
            os.write(("start:"+remoteAddress).getBytes());
            os.write(0x0a);
            os.write(bytebuffer);
            os.write(0x0a);
            os.write(("end:"+remoteAddress).getBytes());
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

