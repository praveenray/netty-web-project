package biz.yellowfish.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.Cookie;
import io.netty.handler.codec.http.CookieDecoder;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.QueryStringDecoder;
import io.netty.util.CharsetUtil;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JaxControllerDispatcher {
    private static Logger log = Logger.getLogger(JaxControllerDispatcher.class.getName());

    private Map<String,Method> methodMap;
    private List<MethodUriPath> urisWithParameters;
    private ObjectMapper jacksonMapper;
    private Utils utils;

    public JaxControllerDispatcher(ObjectMapper jacksonMapper, Utils utils) {
        this.jacksonMapper = jacksonMapper;
        this.urisWithParameters = new ArrayList<>();
        this.methodMap = new HashMap<>();
        this.utils = utils;
    }

    public void init() {
        try {
            Class[] classes = this.utils.get_classes_in_a_package("biz.yellowfish.controllers");
            for (Class cls : classes) {
                Annotation path_annotation = cls.getAnnotation(Path.class);
                if (path_annotation != null) {
                    String prefix = ((String) path_annotation.annotationType().getMethod("value").invoke(path_annotation)).trim().replaceAll("/$", "");
                    for (Method method : cls.getDeclaredMethods()) {
                        if (Modifier.isPublic(method.getModifiers())) {
                            if (method.isAnnotationPresent(Path.class)) {
                                Path path = method.getAnnotation(Path.class);
                                String[] uris = path.value().split("\\s+");
                                for (String uri: uris) {
                                    Object mapping = this.createUriMethodMapping(method, prefix, uri);
                                    if (mapping instanceof String) {
                                        this.methodMap.put(mapping.toString(), method);
                                    } else {
                                        this.urisWithParameters.add((MethodUriPath) mapping);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            for (Map.Entry<String, Method> entry : methodMap.entrySet()) {
                this.log.info(entry.getKey() + " => " + entry.getValue().getName());
            }

            Collections.sort(this.urisWithParameters, new Comparator<MethodUriPath>() {
                @Override
                public int compare(MethodUriPath o1, MethodUriPath o2) {
                    String[] t1 = o1.getOriginalPath().split("/");
                    String[] t2 = o2.getOriginalPath().split("/");
                    return t1.length - t2.length;
                }
            });
            for (MethodUriPath m : this.urisWithParameters) {
                this.log.info(m.toString());
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public HttpResponse handleRequest(FullHttpRequest request, ChannelHandlerContext ctx) throws Exception {
        QueryStringDecoder decoder = new QueryStringDecoder(request.getUri());
        String uri = decoder.path().trim().replaceAll("/$", "");
        HttpMethod method = request.getMethod();
        Object[] methodAndValues = this.pickControllerMethod(method, uri);
        if (methodAndValues == null) {
            return null;
        }
        Method m = (Method) methodAndValues[0];
        if (m == null) {
            throw new IllegalStateException("No Controller method for URI "+uri + " and method "+method);
        }

        Map<String,String> pathValues = (Map<String, String>) methodAndValues[1];
        Map<String,List<String>> queryParams = decoder.parameters();
        return this.callControllerMethod(m, request, pathValues, ctx, queryParams);
    }

    private Object[] pickControllerMethod(HttpMethod httpMethod, String uri) {
        String methodName = null;
        if (httpMethod.equals(HttpMethod.GET)) {
            methodName = HttpMethod.GET.name();
        } else if (httpMethod.equals(HttpMethod.POST)) {
            methodName = HttpMethod.POST.name();
        } else if (httpMethod.equals(HttpMethod.PUT)) {
            methodName = HttpMethod.PUT.name();
        } else if (httpMethod.equals(HttpMethod.DELETE)) {
            methodName = HttpMethod.DELETE.name();
        }

        String fullUri = methodName+uri;
        Method method = this.methodMap.get(fullUri);
        if (method != null) {
            return new Object[]{method, null};
        }

        List<MethodUriPath> matches = new ArrayList<MethodUriPath>();
        final AtomicInteger max = new AtomicInteger(0);
        for (MethodUriPath mpath : this.urisWithParameters) {
            Map<String, String> values = mpath.match(methodName, uri);
            if (values != null) {
                matches.add(mpath);
                if (mpath.getOriginalPathSize() > max.intValue()) {
                    max.set(mpath.getOriginalPathSize());
                }
            }
        }

        Object[] values = null;
        if (matches.size() > 1) {
            // one with largest number of components wins
            int maxInt = max.intValue();
            List<MethodUriPath> filtered = new ArrayList<>(maxInt);
            for (MethodUriPath m : matches) {
                if (m.getOriginalPathSize() == maxInt) {
                    filtered.add(m);
                }
            }
            if (filtered.size() == 1) {
                values = new Object[]{matches.get(0).getMethod(), matches.get(0).match(methodName, uri) };
            } else {
                // one with least number of variables in the path wins
                final AtomicInteger min = new AtomicInteger(Integer.MAX_VALUE);
                for (MethodUriPath m : matches) {
                    if (m.getPathParametersSize() < min.intValue()) {
                        min.set(m.getPathParametersSize());
                    }
                }
                filtered.clear();
                for (MethodUriPath m : matches) {
                    if (m.getOriginalPathSize() == min.intValue()) {
                        filtered.add(m);
                    }
                }
                if (filtered.size() > 1) {
                    throw new IllegalArgumentException("Uri "+ uri + " and method " + methodName + " matches more than one Path templates");
                }
                values = new Object[]{matches.get(0).getMethod(), matches.get(0).match(methodName, uri) };
            }
        } else if (matches.size() == 1) {
            values = new Object[]{matches.get(0).getMethod(), matches.get(0).match(methodName, uri) };
        }

        return values;
    }

    private HttpResponse callControllerMethod(Method m, FullHttpRequest request, Map<String,String> pathValues, ChannelHandlerContext ctx, Map<String,List<String>> queryParams) {
        try {
            Object controller = m.getDeclaringClass().newInstance();
            Object response = m.invoke(controller, this.convertParameters(m, request, ctx.channel(), pathValues, queryParams));
            FullHttpResponse http_response = null;
            String content_type = this.get_produces_value_from_annotation(m);
            if (response instanceof FullHttpResponse) {
                http_response = (FullHttpResponse) response;
            } else {
                if (!(response instanceof String) && content_type.equals(MediaType.APPLICATION_JSON)) {
                    response = this.jacksonMapper.writeValueAsString(response);
                }
                if (response instanceof String) {
                    byte[] bytes = ((String) response).getBytes();
                    ByteBuf buffer = Unpooled.copiedBuffer(bytes);
                    http_response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, buffer);
                    http_response.headers().set(HttpHeaders.Names.CONTENT_TYPE, content_type);
                    http_response.headers().set(HttpHeaders.Names.CONTENT_LENGTH, bytes.length);
                } else {
                    String response_class = response == null ? "": response.getClass().getName();
                    throw new IllegalStateException("Do not know how to handle output from ["+m.getDeclaringClass().getName() + ":"+ m.toString()+"], "+response_class);
                }
            }
            return http_response;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String get_produces_value_from_annotation(Method method) {
        Produces annotation = method.getAnnotation(Produces.class);
        String value = null;
        if (annotation != null) {
            String[] values = annotation.value();
            if (values != null && values.length > 0) {
                value = values[0];
            }
        }
        if (value == null || value.trim().length() == 0) {
            value = MediaType.APPLICATION_JSON;
        }
        return value;
    }

    private Object[] convertParameters(Method method, FullHttpRequest request, Channel channel, Map<String,String> pathValues, Map<String, List<String>> parameters) {
        try {
            Object[] argValues = new Object[method.getGenericParameterTypes().length];
            Class[] parameterTypes = method.getParameterTypes();
            Annotation[][] annotations = method.getParameterAnnotations();
            int paramCount = parameterTypes.length;
            for (int i=0; i < paramCount; i++) {
                if (parameterTypes[i].equals(Channel.class)) {
                    argValues[i] = channel;
                } else if (parameterTypes[i].equals(HttpRequest.class)) {
                    argValues[i] = request;
                } else {
                    String source = null;
                    QueryParam qn = this.getParameterAnnotation(annotations[i], QueryParam.class);
                    FormParam fn = this.getParameterAnnotation(annotations[i], FormParam.class);
                    PathParam pathParam = null;
                    CookieParam cookieParam = null;
                    if (qn != null || fn != null) {
                        String paramName = qn != null ? qn.value() : fn.value();
                        List<String> paramValues = parameters.get(paramName);
                        if (paramValues != null && paramValues.size() > 0) {
                            Class parameterClass = parameterTypes[i];
                            source = (parameterClass.equals(List.class) || parameterClass.equals(Set.class)) ? this.csv_join(paramValues) : paramValues.get(0);
                        } else {
                            DefaultValue df = this.getParameterAnnotation(annotations[i], DefaultValue.class);
                            source = df == null ? null : df.value();
                        }
                    } else if ((pathParam = this.getParameterAnnotation(annotations[i], PathParam.class)) != null) {
                        String paramName = pathParam.value();
                        source = pathValues.get(paramName);
                    } else if ((cookieParam = this.getParameterAnnotation(annotations[i], CookieParam.class)) != null) {
                        Cookie cookie = this.findCookie(request, cookieParam.value());
                        source = cookie == null ? null : cookie.getValue();
                    } else {
                        // read POST body, if any
                        if (request.getMethod().equals(HttpMethod.POST)) {
                            ByteBuf buf = request.content();
                            if (buf != null && buf.isReadable()) {
                                source = buf.toString(CharsetUtil.UTF_8);
                            }
                        }
                    }

                    if (source == null) {
                        Required reqd = this.getParameterAnnotation(annotations[i], Required.class);
                        if (reqd != null) {
                            throw new IllegalArgumentException("Parameter "+i+" for method "+method.getName()+" is Required");
                        }
                        this.log.info("Parameter "+i+" for method "+method.getName()+" has no incoming value and no DefaultValue Annotation");
                        argValues[i] = null;
                    }
                    try {
                        argValues[i] = this.convertParameterValue(source, parameterTypes[i]);
                    } catch (Exception e) {
                        throw new IllegalArgumentException("Conversion Failed. Parameter number "+i+" method "+method.getName()+":"+e.getMessage());
                    }
                }
            }
            return argValues;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private  Object convertParameterValue(String source, Class target)  throws Exception {
        if (source == null || source.trim().length() == 0) {
            return null;
        }
        source = source.trim();
        if (target.equals(String.class)) {
            return source;
        }
        if (target.equals(int.class) || target.equals(Integer.class)) {
            return Integer.parseInt(source);
        }

        if (target.equals(double.class) || target.equals(Double.class)) {
            return Double.parseDouble(source);
        }

        if (target.equals(Date.class)) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            return format.parse(source);
        }

        return this.jacksonMapper.readValue(source,target);
    }

    private String csv_join(Collection<String> coll) {
        StringBuilder b= new StringBuilder();
        for (String s : coll) {
            b.append(s).append(",");
        }
        return b.toString().replaceAll(",$","");
    }

    private Cookie findCookie(HttpRequest request, String cookieName) {
        String cookieHeader = request.headers().get(HttpHeaders.Names.COOKIE);
        if (cookieHeader != null) {
            Set<Cookie> cookieJar = CookieDecoder.decode(cookieHeader);
            for (Cookie cookie : cookieJar) {
                if (cookie.getName().equals(cookieName)) {
                    return cookie;
                }
            }
        }
        return null;
    }

    private <T extends Annotation> T getParameterAnnotation(Annotation[] annotations, Class<T> lookingFor) {
        int len = annotations.length;
        for (int i=0; i < len; i++) {
            Annotation annotation = annotations[i];
            if (lookingFor.isInstance(annotation)) {
                return (T) annotation;
            }
        }
        return null;
    }

    private Object createUriMethodMapping(Method method, String prefix, String uri) {
        if (uri.contains("{")) {
            return new MethodUriPath(method, prefix);
        } else {
            String annotationName = this.getMethodAnnotationName(method);
            return (annotationName+prefix+uri).replaceAll("/$","");
        }
    }

    public static String getMethodAnnotationName(Method method) {
        String annotationName = null;
        if (method.isAnnotationPresent(GET.class)) {
            annotationName = HttpMethod.GET.name();
        } else if (method.isAnnotationPresent(POST.class)) {
            annotationName = HttpMethod.POST.name();
        } else if (method.isAnnotationPresent(DELETE.class)) {
            annotationName = HttpMethod.DELETE.name();
        } else if (method.isAnnotationPresent(PUT.class)) {
            annotationName = HttpMethod.PUT.name();
        }
        return annotationName;
    }

    class MethodUriPath {
        private Pattern regularExpression;
        private String originalPath;
        private Method method;
        private List<String> pathParameterNames;
        private String httpMethod;
        private String classUri;
        private int originalPathSize;

        public MethodUriPath(Method method, String prefix) {
            String path = method.getAnnotation(Path.class).value().trim();
            if (!path.startsWith("/")) {
                throw new IllegalArgumentException("Path value for method "+method.getName()+" does not start with '/'");
            }
            this.method = method;
            this.originalPath = path;
            this.originalPathSize = path.split("/").length;
            this.httpMethod = JaxControllerDispatcher.getMethodAnnotationName(method);
            Object[] extract = this.extractRegularExpress(path.replaceAll("/$",""));
            this.regularExpression = Pattern.compile("^"+(prefix + (String) extract[0])+"$");
            this.pathParameterNames = (List<String>) extract[1];
            this.classUri = prefix;
        }

        public Map<String, String> match(String httpMethodName, String incomingUri) {
            if (!this.httpMethod.equals(httpMethodName)) {
                return null;
            }

            Matcher matcher = this.regularExpression.matcher(incomingUri);
            List<String> groups = new ArrayList<String>();
            boolean matches = false;
            if (matcher.find()) {
                for (int i = 1; i <= matcher.groupCount(); i++) {
                    groups.add(matcher.group(i));
                }
                matches = true;
            }
            if (matches) {
                if (groups.size() != this.pathParameterNames.size()) {
                    throw new IllegalArgumentException("Number of Path Parameters "+ groups.size() +" does not match expected number "+ this.pathParameterNames.size());
                }
                Map<String, String> parameterValues = new HashMap<String,String>();
                int i = 0;
                for (String name : this.pathParameterNames) {
                    parameterValues.put(name, groups.get(i++));
                }
                return parameterValues;
            }
            return null;
        }

        private Object[] extractRegularExpress(String path) {
            List<String> groups = new ArrayList<String>();
            if (path.contains("{")) {
                String rePath = path;
                Pattern p = Pattern.compile("\\{(.+?)\\}");
                Matcher m = p.matcher(path);
                while (m.find()) {
                    String group = m.group(0);
                    rePath = rePath.replace(group, "(.+?)");
                    groups.add(m.group(1));
                /*
                if (path.endsWith(group)) {
                    rePath = rePath + "$"; // for paths ending in {id}, we need (.+?)$ so the re eats all remaining characters
                }
                */
                }
                return new Object[] {
                        rePath,
                        groups
                };
            }
            throw new IllegalArgumentException("Path does not contain '{' character");
        }

        public Method getMethod() {
            return method;
        }

        public String getOriginalPath() {
            return originalPath;
        }

        public int getOriginalPathSize() {
            return originalPathSize;
        }

        public int getPathParametersSize() {
            return this.pathParameterNames.size();
        }

        @Override
        public String toString() {
            return this.httpMethod+":"+this.classUri+":"+this.originalPath+":"+this.regularExpression+":"+this.getPathParametersSize();
        }
    }

}
