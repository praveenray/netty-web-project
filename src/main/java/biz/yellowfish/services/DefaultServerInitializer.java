package biz.yellowfish.services;

import biz.yellowfish.handlers.ControllerManager;
import biz.yellowfish.handlers.ExceptionHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.logging.LoggingHandler;

public class DefaultServerInitializer extends ChannelInitializer<SocketChannel> {
    private JaxControllerDispatcher cm;
    public DefaultServerInitializer() {
        this.cm = new JaxControllerDispatcher(new ObjectMapper(), new Utils());
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new HttpRequestDecoder())
                .addLast(new HttpObjectAggregator(10*1024))
                .addLast(new LoggingHandler())
                .addLast(new HttpResponseEncoder())
                .addLast(new ControllerManager(this.cm))
                .addLast(new ExceptionHandler());
    }
}

/*
Alternate pipeline
 ChannelPipeline pipeline = Channels.pipeline();
        pipeline.addLast("readRequest", new HttpRequestDecoder());
        pipeline.addLast("ignore-favicon", new FaviconHandler());
        pipeline.addLast("aggregator", new HttpChunkAggregator(10*1024*1024));
        pipeline.addLast("serve-statics", this.getStaticHandler());
        pipeline.addLast("read-http-params", new ReadHttpParamsHandler());
        pipeline.addLast("printDetails", new ShowRequestDetailsHandler());
        pipeline.addLast("authenticator", this.getAuthenticationHandler());
        pipeline.addLast("encoder", new HttpResponseEncoder());
        pipeline.addLast("chunk-writer", new ChunkedWriteHandler());
        pipeline.addLast("controllers", this.getControllerManagerHandler());
        pipeline.addLast("no-mapping", new NoMappingHandler());
 */
