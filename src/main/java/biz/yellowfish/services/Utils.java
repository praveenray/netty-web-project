package biz.yellowfish.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    public String[] get_class_names_in_a_package(String pkg) throws Exception {
        String pkg_slashes = pkg.replaceAll("\\.","/");
        URL pkg_url = this.getClass().getClassLoader().getResource(pkg_slashes);
        BufferedReader reader = new BufferedReader(new InputStreamReader(pkg_url.openStream()));
        String line = null;
        List<String> classes = new ArrayList<>();
        while ((line = reader.readLine()) != null) {
            classes.add((pkg_slashes + "."+ line).replaceAll("/",".").replaceAll("\\.class$",""));
        }
        return classes.toArray(new String[]{});
    }

    public Class[] get_classes_in_a_package(String pkg) throws Exception {
        ClassLoader loader = this.getClass().getClassLoader();
        String[] classNames = this.get_class_names_in_a_package(pkg);
        Class[] classes = new Class[classNames.length];
        int i=0;
        for (String clsName : classNames) {
            classes[i++] = loader.loadClass(clsName);
        }
        return classes;
    }
}
