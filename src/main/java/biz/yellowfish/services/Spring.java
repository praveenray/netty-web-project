package biz.yellowfish.services;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Spring {
    private static ApplicationContext context;
    public static ApplicationContext getContext() { return context;}
    public void start() {
        context = new FileSystemXmlApplicationContext("classpath:spring.xml");
        System.out.println("Loaded Spring context from "+context.getDisplayName()+". Ready.");
    }

    public void stop() {
        ((FileSystemXmlApplicationContext) context).close();
        System.out.println(context.getDisplayName() + " stopped");
    }
}
