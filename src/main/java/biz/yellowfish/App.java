package biz.yellowfish;

import biz.yellowfish.services.DefaultServerInitializer;
import biz.yellowfish.services.Spring;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.apache.commons.logging.Log;

import java.io.IOException;
import java.util.logging.Logger;

public class App
{
    private static Logger log = Logger.getLogger(App.class.getName());

    public static void main( String[] args ) throws IOException {
        int port = Integer.parseInt(args[0]);
        new App().start_server(port);
    }

    private void start_server(int port) {
        EventLoopGroup boss = new NioEventLoopGroup(1);
        EventLoopGroup worker = new NioEventLoopGroup();
        try {
            DefaultServerInitializer initializer = new DefaultServerInitializer();
            ServerBootstrap b = new ServerBootstrap();
            b.group(boss, worker)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG, 10)
                    .option(ChannelOption.SO_REUSEADDR, true);
            Channel ch = b.bind(port).sync().channel();
            log.info("Listening on: "+port);
            ch.closeFuture().sync();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
